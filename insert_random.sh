#!/bin/bash
info() { printf "[INFO] (%s) %s\n" "$( date --iso-8601=s )" "$*" >&2; }
calc_date() {
	date -ud @$(date -u +$(($(date --utc +"%s") + $1))) --iso-8601=s
}
TEST_UID=$(uuidgen)

export PGPASSWORD="postgres"
export PGUSER="postgres"
export PGHOST="localhost"
ACTIVITY_NAME=$(head -c 30 /dev/random | base32)
TIME_NOW=$(calc_date 0)

info "Cleaning db"

psql -d iqiper -c "DELETE FROM iqiper.distant_user;"

info "Inserting user $TEST_UID"

psql -d iqiper -c "INSERT INTO iqiper.distant_user(id) VALUES ('$TEST_UID');"

TEST_ACTIVITY_ID=$(uuidgen)
TEST_ACTIVITY_ID2=$(uuidgen)

info "Inserting activity $TEST_ACTIVITY_ID"

psql -d iqiper -t -c "INSERT INTO iqiper.activity(
	id,
	uid,
	name,
	start_date
	)
	VALUES 
	(
	'$TEST_ACTIVITY_ID',
	'$TEST_UID',
	'$ACTIVITY_NAME',
	'$TIME_NOW'
	)"

info "Inserting activity 2 $TEST_ACTIVITY_ID2"

psql -d iqiper -t -c "INSERT INTO iqiper.activity(
	id,
	uid,
	name,
	start_date
	)
	VALUES 
	(
	'$TEST_ACTIVITY_ID2',
	'$TEST_UID',
	'$ACTIVITY_NAME',
	'$TIME_NOW'
	)"


TEST_CONTACT_ID=$(uuidgen)
TEST_CONTACT_NICKNAME=$(head -c 30 /dev/random | base32)
TEST_CONTACT_EMAIL=$(echo -n "$(head -c 30 /dev/random | base32)@$(head -c 30 /dev/random | base32).com")

info "Inserting contact $TEST_CONTACT_ID"

psql -d iqiper -t -c "INSERT INTO iqiper.contact(
	id,
	uid,
	nickname,
	email,
	type
	)
	VALUES 
	(
	'$TEST_CONTACT_ID',
	'$TEST_UID',
	'$TEST_CONTACT_NICKNAME',
	'$TEST_CONTACT_EMAIL',
	'EMAIL'
	)"

info "Inserting activity contact $TEST_ACTIVITY_ID-$TEST_CONTACT_ID"

psql -d iqiper -c "INSERT INTO iqiper.activity_contact(activity_id, contact_id) VALUES (
	'$TEST_ACTIVITY_ID',
	'$TEST_CONTACT_ID'
	);"


generate_constraint() {
	info "Inserting activity constraint $1 ($2 < $3 < $4)"
	TEST_ACTIVITY_CONSTRAINT_START_DATE=$(calc_date $2)
	TEST_ACTIVITY_CONSTRAINT_CHECK_DATE=$(calc_date $3)
	TEST_ACTIVITY_CONSTRAINT_END_DATE=$(calc_date $4)

	psql -d iqiper -t -c "INSERT INTO iqiper.activity_constraint(
		id,
		activity_id,
		start_date,
		notify_date,
		end_date,
		type
		)
		VALUES 
		(
		'$1',
		'$TEST_ACTIVITY_ID',
		'$TEST_ACTIVITY_CONSTRAINT_START_DATE',
		'$TEST_ACTIVITY_CONSTRAINT_CHECK_DATE',
		'$TEST_ACTIVITY_CONSTRAINT_END_DATE',
		'TIMER'
		)"
}

generate_constraint "$(uuidgen)" 0 10 20
generate_constraint "$(uuidgen)" 0 20 30
generate_constraint "$(uuidgen)" 0 30 40
generate_constraint "$(uuidgen)" 10 30 40
generate_constraint "$(uuidgen)" 20 30 40
generate_constraint "$(uuidgen)" -120 -70 -60
generate_constraint "$(uuidgen)" 0 3600 3300
generate_constraint "$(uuidgen)" 60 3300 3600
generate_constraint "$(uuidgen)" 120 3300 3600
generate_constraint "$(uuidgen)" 180 3300 3600
generate_constraint "$(uuidgen)" 240 3300 3600
generate_constraint "$(uuidgen)" 300 3300 3600
