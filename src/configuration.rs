use crate::errors::InternalError;
use serde::Deserialize;
use visage::configuration as visage_config;
#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationOeil {
    pub bouche_backlog: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Configuration {
    pub visage: visage_config::Configuration,
    pub server: peau::configuration::ConfigurationServer,
    pub oeil: ConfigurationOeil,
}

/// Read the configuration from the provided path
pub fn get(path: &std::path::Path, env_var: &str) -> Result<Configuration, InternalError> {
    let mut settings = config::Config::default();
    settings
        .merge(config::File::from(path))?
        .merge(config::Environment::with_prefix(env_var).separator("__"))?;
    Ok(settings.try_into::<Configuration>()?)
}
