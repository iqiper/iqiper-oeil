use super::oeil::{Oeil, OeilDBCheckType};
use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use test_db::*;

#[test]
fn no_constraint() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("The oeil should've been created");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Import should'nt fail");
            assert_eq!(res, 0, "There should be no imported constraints");
            clean_up(&mut oeil_test).await;
        });
        Ok(())
    });
}

#[test]
fn constraint_in_the_past() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(1)),
                None,
            );
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("The oeil should've been created");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Import should'nt fail");
            assert_eq!(res, 0, "There should be no imported constraints");
            clean_up(&mut oeil_test).await;
        });
        Ok(())
    });
}

#[test]
fn constraint_begin_in_the_past_finish_in_the_future() {
    let db_conn = test_db::establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(1)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Import shouldn't fail");
            assert_eq!(res, 1, "There should a single imported constraints");
            let redis_verif: bool =
                redis::Cmd::hexists(crate::oeil::YEUX_BACKLOG, constraint.to_string())
                    .query_async(&mut oeil_test.test_conn)
                    .await
                    .expect("The redis verification shouldn't fail");
            assert_eq!(
                redis_verif, true,
                "The member of the `yeux` list should be the created constraint"
            );
            clean_up(&mut oeil_test).await;
        });
        Ok(())
    });
}

#[test]
fn constraint_begin_in_the_future_finish_in_the_future() {
    let db_conn = test_db::establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() + Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Import shouldn't fail");
            assert_eq!(res, 0, "There shouldn't be any imported constraints");
            clean_up(&mut oeil_test).await;
        });
        Ok(())
    });
}

#[test]
fn multiple_constraints() {
    let db_conn = test_db::establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = vec![
                create_test_user(&db_conn),
                create_test_user(&db_conn),
                create_test_user(&db_conn),
            ];
            let activity_id = {
                let mut res = vec![];
                for (_i, _uid) in uid.iter().enumerate() {
                    for i in 0..3 {
                        res.push(create_activity(
                            &db_conn,
                            &uid[i],
                            false,
                            Some(Utc::now() - Duration::minutes(1)),
                            None,
                        ));
                    }
                }
                res
            };
            let constraint = {
                let mut res = vec![];
                for (i, id) in activity_id.iter().enumerate() {
                    for _y in 0..1 {
                        res.push(create_activity_constraint(
                            &db_conn,
                            &uid[i % 3],
                            &id,
                            false,
                            Some(Utc::now() - Duration::minutes(1)),
                            None,
                            Some(Utc::now() + Duration::minutes(2)),
                            None,
                        ));
                    }
                }
                res
            };
            let mut constraint: Vec<String> =
                constraint.into_iter().map(|x| x.to_string()).collect();
            constraint.sort();
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Import shouldn't fail");
            let mut redis_verif: Vec<String> = redis::Cmd::hkeys(oeil::YEUX_BACKLOG)
                .query_async(&mut oeil_test.test_conn)
                .await
                .expect("redis should've listed the backlog correctly");
            redis_verif.sort();
            assert_eq!(res, 9, "There should be a lot of imported constraints");
            assert_eq!(
                constraint, redis_verif,
                "The list of constraints should be equal"
            );
            // clean_up(&mut oeil_test).await;
        });
        Ok(())
    });
}
