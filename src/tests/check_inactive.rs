use super::oeil::{Oeil, OeilDBCheckType};
use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use test_db::*;

#[test]
fn inactive_no_present_in_oeil() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let _id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            master_oeil
                .master_set_time_check(
                    oeil::YEUX_INACTIVE_UPDATE_TS,
                    Utc::now() - Duration::seconds(30),
                )
                .await
                .expect("the timestamps should've been set");
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Inactive, &db_conn)
                .await
                .expect("Check shouldn't fail");
            assert_eq!(res, 1, "There should be a single imported constraints");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn active_present_in_oeil() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            master_oeil
                .set_assigned_oeil(&id, OeilTaskStatus::Active(*master_oeil.visage().id()))
                .await
                .expect("setting the jobs in the backlog");
            master_oeil
                .master_set_time_check(
                    oeil::YEUX_INACTIVE_UPDATE_TS,
                    Utc::now() - Duration::seconds(30),
                )
                .await
                .expect("the timestamps should've been set");
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Inactive, &db_conn)
                .await
                .expect("Check shouldn't fail");
            assert_eq!(res, 1, "There should be a single imported constraints");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn inactive_present_in_oeil() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            master_oeil
                .set_assigned_oeil(&id, OeilTaskStatus::Inactive)
                .await
                .expect("setting the jobs in the backlog");
            master_oeil
                .master_set_time_check(
                    oeil::YEUX_INACTIVE_UPDATE_TS,
                    Utc::now() - Duration::seconds(30),
                )
                .await
                .expect("the timestamps should've been set");
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Inactive, &db_conn)
                .await
                .expect("Check shouldn't fail");
            assert_eq!(res, 1, "There should be a single imported constraints");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn backup_timestamps_only() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let _id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            master_oeil
                .master_set_time_check(
                    oeil::YEUX_FULL_UPDATE_TS,
                    Utc::now() - Duration::seconds(30),
                )
                .await
                .expect("the timestamps should've been set");
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Inactive, &db_conn)
                .await
                .expect("Check shouldn't fail");
            assert_eq!(res, 1, "There should be a single imported constraints");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn no_timestamps() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let _id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            let res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Inactive, &db_conn)
                .await
                .expect("Check shouldn't fail");
            assert_eq!(res, 0, "There shouldn't be a single imported constraints");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}
