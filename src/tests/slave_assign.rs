use super::oeil::{Oeil, OeilDBCheckType};
use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use test_db::*;
use uuid::Uuid;

#[test]
fn simple_new() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            assert_eq!(*res.added(), 1, "There should be 1 assigned job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn new_but_bad_formatted_id() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let _e: usize = redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil.visage().id()),
                "BLABBLA",
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .unwrap();
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            assert_eq!(*res.added(), 1, "There should be 1 assigned job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn new_but_not_found() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let _e: usize = redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil.visage().id()),
                Uuid::new_v4().to_string(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .unwrap();
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            assert_eq!(*res.added(), 1, "There should be 1 assigned job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn update_normal() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            assert_eq!(*res.added(), 1, "There should be 1 assigned job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            let _e: usize = redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil.visage().id()),
                constraint_id.to_string(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .unwrap();
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            assert_eq!(*res.added(), 0, "There should be 0 assigned job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn update_not_found() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            assert_eq!(*res.added(), 1, "There should be 1 assigned job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            db::activity_constraint::delete(&db_conn, &uid, &activity_id, &constraint_id)
                .expect("to delete the constraint form the db");
            let _e: usize = redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil.visage().id()),
                constraint_id.to_string(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .unwrap();
            let _e = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch new jobs");
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::Done)
                .await
                .expect("To fetch done jobs");
            assert_eq!(*res.added(), 0, "There should be 0 added job");
            assert_eq!(*res.removed(), 1, "There should be 1 removed job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, false, "should've been false #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn done_normal() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            assert_eq!(*res.added(), 1, "There should be 1 assigned job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            oeil.set_assigned_oeil(&constraint_id, OeilTaskStatus::Done(*oeil.visage().id()))
                .await
                .unwrap();
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::Done)
                .await
                .expect("To fetch done jobs");
            assert_eq!(*res.removed(), 1, "There should be 1 removed job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, false, "should've been false #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn done_also_update() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            assert_eq!(*res.added(), 1, "There should be 1 assigned job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            oeil.set_assigned_oeil(&constraint_id, OeilTaskStatus::Done(*oeil.visage().id()))
                .await
                .unwrap();
            let _e: bool = redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil.visage().id()),
                constraint_id.to_string(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .unwrap();
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::Done)
                .await
                .expect("To fetch done jobs");
            assert_eq!(*res.removed(), 1, "There should be 1 removed job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .exists(YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, false, "should've been false #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn done_bad_formatted() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let _e: usize = redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_DONE_FORMAT!(oeil.visage().id()),
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .unwrap();
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let _res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch new jobs");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::Done)
                .await
                .expect("To fetch done jobs");
            assert_eq!(*res.removed(), 0, "There should be 0 removed job");
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn done_not_found() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let _e: usize = redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_DONE_FORMAT!(oeil.visage().id()),
                Uuid::new_v4().to_string(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .unwrap();
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let _res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch new jobs");
            let res = oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::Done)
                .await
                .expect("To fetch done jobs");
            assert_eq!(*res.removed(), 0, "There should be 0 removed job"); // Actually no job have been removed, but it would take an extra step to check that
            let redis_verif: Vec<bool> = redis::pipe()
                .exists(YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()))
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            for (i, v) in redis_verif.into_iter().enumerate() {
                assert_eq!(v, true, "should've been true #{}", i);
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}
