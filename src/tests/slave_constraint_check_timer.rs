use super::oeil::{Oeil, OeilDBCheckType};
use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use std::str::FromStr;
use test_db::*;
use uuid::Uuid;

#[test]
fn not_yet_started() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() + Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.set_assigned_oeil(&constraint_id, OeilTaskStatus::Inactive)
                .await
                .expect("To assign the job");
            let assignation: OeilTDResult = oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(*assignation.added(), 1, "There should be 1 added job");
            oeil.slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            let res: OeilTaskResult = oeil
                .slave_run_tasks_internal(&db_conn)
                .await
                .expect("to run the constraints checks");
            assert_eq!(*res.skipped(), 1, "There should be 1 skipped");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn alerted() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let _constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let assignation: OeilTDResult = oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(*assignation.added(), 1, "There should be 1 added job");
            oeil.slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            let res: OeilTaskResult = oeil
                .slave_run_tasks_internal(&db_conn)
                .await
                .expect("to run the constraints checks");
            assert_eq!(*res.notified(), 1, "There should be 1 notified");
            let res: (usize, String) = redis::pipe()
                .llen(oeil.config().bouche_backlog.as_str())
                .lpop(oeil.config().bouche_backlog.as_str())
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            assert_eq!(res.0, 1, "There should be 1 element in the bouche backlog");
            Uuid::from_str(res.1.as_str()).expect("to parse an uuid");
            Ok(())
        })
    });
}

#[test]
fn alerted_but_not_twice() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let _constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let assignation: OeilTDResult = oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(*assignation.added(), 1, "There should be 1 added job");
            oeil.slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            let res: OeilTaskResult = oeil
                .slave_run_tasks_internal(&db_conn)
                .await
                .expect("to run the constraints checks");
            assert_eq!(*res.notified(), 1, "There should be 1 notified");
            let res: OeilTaskResult = oeil
                .slave_run_tasks_internal(&db_conn)
                .await
                .expect("to run the constraints checks");
            assert_eq!(*res.notified(), 0, "There should be 0 notified");
            let res: (usize, String) = redis::pipe()
                .llen(oeil.config().bouche_backlog.as_str())
                .lpop(oeil.config().bouche_backlog.as_str())
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            assert_eq!(res.0, 1, "There should be 1 element in the bouche backlog");
            Uuid::from_str(res.1.as_str()).expect("to parse an uuid");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn failed() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            oeil.set_assigned_oeil(&constraint_id, OeilTaskStatus::Inactive)
                .await
                .expect("To assign the job");
            let assignation: OeilTDResult = oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(*assignation.added(), 1, "There should be 1 added job");
            oeil.slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            let res: OeilTaskResult = oeil
                .slave_run_tasks_internal(&db_conn)
                .await
                .expect("to run the constraints checks");
            assert_eq!(*res.failed(), 1, "There should be 1 failed");
            let res: (usize, String) = redis::pipe()
                .llen(oeil.config().bouche_backlog.as_str())
                .lpop(oeil.config().bouche_backlog.as_str())
                .query_async(&mut oeil_test.test_conn)
                .await
                .unwrap();
            assert_eq!(res.0, 1, "There should be 1 element in the bouche backlog");
            Uuid::from_str(res.1.as_str()).expect("to parse an uuid");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn bad_formatted_key() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let _activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            let _e: usize = redis::Cmd::hset(
                YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                "AAAAAAAAAAAAAAAA",
                serde_json::to_string(&OeilTask::new(OeilTaskStatus::Inactive)).unwrap(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .expect("To run the redis cmd");
            oeil.slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            let res: OeilTaskResult = oeil
                .slave_run_tasks_internal(&db_conn)
                .await
                .expect("to run the constraints checks");
            assert_eq!(
                *res.badly_formatted(),
                1,
                "There should be 1 badly_formatted"
            );
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn bad_formatted_body() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let _activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            let _e: usize = redis::Cmd::hset(
                YEUX_OEIL_BACKLOG_FORMAT!(oeil.visage().id()),
                Uuid::new_v4().to_string(),
                "AAAAAAAAAAAA",
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .expect("To run the redis cmd");
            oeil.slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            let res: OeilTaskResult = oeil
                .slave_run_tasks_internal(&db_conn)
                .await
                .expect("to run the constraints checks");
            assert_eq!(
                *res.badly_formatted(),
                1,
                "There should be 1 badly_formatted"
            );
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                Some(Utc::now() + Duration::seconds(30)),
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            oeil.visage_mut()
                .register_lease()
                .await
                .expect("to register the lease");
            oeil.master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let assignation: OeilTDResult = oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(*assignation.added(), 1, "There should be 1 added job");
            oeil.slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To fetch missing jobs");
            let res: OeilTaskResult = oeil
                .slave_run_tasks_internal(&db_conn)
                .await
                .expect("to run the constraints checks");
            assert_eq!(*res.success(), 1, "There should be 1 success");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}
