use super::*;
use async_std::task;
use once_cell::sync::Lazy;
use std::sync::{Mutex, MutexGuard};
mod check_active;
mod check_inactive;
mod orphaned_oeil;
mod read_all_constraints;
mod slave_assign;
mod slave_constraint_check_timer;
mod tasks_distribution;
use crate::oeil::*;
use std::ops::Deref;
const NB_REDIS_DB: u8 = 16;

static OEIL_METRICS: Lazy<OeilMetrics> = Lazy::new(|| OeilMetrics::new().unwrap());

pub struct OeilTest {
    pub db: MutexGuard<'static, u8>,
    pub config: crate::configuration::Configuration,
    pub test_client: redis::Client,
    pub test_conn: redis::aio::ConnectionManager,
    pub db_conn: diesel::PgConnection,
}

pub async fn setup() -> OeilTest {
    let db = lock_db().await;
    let mut config = get_config();
    let db_conn = test_db::establish_connection();
    config.visage.redis.database = Some((*db).to_string());
    let (test_client, mut test_conn) =
        visage::connections::redis::get_redis_connection(&config.visage.redis)
            .await
            .expect("The connection failed to be established");
    visage::connections::redis::test_connection(&mut test_conn)
        .await
        .expect("The connection is invalid");
    let flush_res: bool = redis::cmd("FLUSHDB")
        .query_async(&mut test_conn)
        .await
        .expect("Flushed fails");
    assert_eq!(flush_res, true, "Clean up wen't wrong");
    OeilTest {
        db,
        config,
        test_client,
        test_conn,
        db_conn,
    }
}

pub async fn clean_up(oeil_setup: &mut OeilTest) {
    let res: bool = redis::cmd("FLUSHDB")
        .query_async(&mut oeil_setup.test_conn)
        .await
        .expect("Flushed fails");
    assert_eq!(res, true, "Clean up went wrong");
}

static DB_POOL: Lazy<Vec<Mutex<u8>>> = Lazy::new(|| {
    let mut res: Vec<Mutex<u8>> = vec![];
    for i in 0..NB_REDIS_DB {
        res.push(Mutex::new(i));
    }
    res
});

pub fn get_config() -> crate::configuration::Configuration {
    crate::configuration::get(
        std::path::Path::new(
            std::env::var(CONF_FILE)
                .unwrap_or("./config.yml".to_string())
                .as_str(),
        ),
        ENV_CONF_VAR,
    )
    .unwrap()
}

pub async fn lock_db() -> MutexGuard<'static, u8> {
    loop {
        for db in DB_POOL.iter() {
            let tmp = db.try_lock();
            if tmp.is_ok() {
                return tmp.unwrap();
            }
        }
        task::sleep(std::time::Duration::from_millis(200)).await;
    }
}
