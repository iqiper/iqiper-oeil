use super::oeil::{Oeil, OeilDBCheckType};
use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use test_db::*;
use uuid::Uuid;

fn check_assigned(status: &OeilTaskStatus) -> Option<Uuid> {
    match status {
        OeilTaskStatus::Active(oeil_id) | OeilTaskStatus::Done(oeil_id) => Some(*oeil_id),
        OeilTaskStatus::Inactive => None,
    }
}

#[test]
fn simple_task_assignation() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            let _res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let assigned_oeil: Option<Uuid> = check_assigned(
                &master_oeil
                    .get_assigned_oeil(&constraint_id)
                    .await
                    .expect("should get the assigned status of the task")
                    .unwrap(),
            );
            assert_eq!(
                assigned_oeil.is_none(),
                true,
                "The task should exists but not assigned"
            );
            let score_before: usize =
                redis::Cmd::zincr(YEUX, master_oeil.visage().id().to_string(), 1 as i64)
                    .query_async(&mut oeil_test.test_conn)
                    .await
                    .expect("to increment the oeil score in redis");
            let assignation_result: OeilTDResult = master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(
                assignation_result.added(),
                &1,
                "it should've assigned 1 task"
            );
            let score_after: usize =
                redis::Cmd::zscore(YEUX, master_oeil.visage().id().to_string())
                    .query_async(&mut oeil_test.test_conn)
                    .await
                    .expect("to increment the oeil score in redis");
            if !(score_after > score_before) {
                panic!("The score should have increased");
            }
            let assigned_oeil: Option<Uuid> = check_assigned(
                &master_oeil
                    .get_assigned_oeil(&constraint_id)
                    .await
                    .expect("should get the assigned status of the task")
                    .unwrap(),
            );
            assert_eq!(
                assigned_oeil.is_some(),
                true,
                "The task should be assigned yet"
            );
            assert_eq!(
                assigned_oeil.unwrap(),
                *master_oeil.visage().id(),
                "The task uuid don't match"
            );
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn active_on_a_dead_oeil() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            let _res = master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            master_oeil
                .set_assigned_oeil(&constraint_id, OeilTaskStatus::Active(Uuid::new_v4()))
                .await
                .expect("should get the assigned status of the task");
            let assignation_result: OeilTDResult = master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(
                assignation_result.updated(),
                &1,
                "it should've updated 1 task"
            );
            let assigned_oeil: Option<Uuid> = check_assigned(
                &master_oeil
                    .get_assigned_oeil(&constraint_id)
                    .await
                    .expect("should get the assigned status of the task")
                    .unwrap(),
            );
            assert_eq!(
                assigned_oeil.is_none(),
                true,
                "The task shouldn't be assigned yet"
            );
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn check_update_backlog_oeil_on_assigned() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            master_oeil
                .set_assigned_oeil(
                    &constraint_id,
                    OeilTaskStatus::Active(*master_oeil.visage().id()),
                )
                .await
                .expect("should get the assigned status of the task");
            let assignation_result: OeilTDResult = master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(
                assignation_result.updated(),
                &1,
                "it should've updated 1 task"
            );
            let exists: bool = redis::Cmd::sismember(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(master_oeil.visage().id()),
                constraint_id.to_string(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .expect("should check if record exists in set");
            assert_eq!(exists, true, "It should exists in the BLU");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn second_pass_on_normal_distribution() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            master_oeil
                .master_db_check_internal(OeilDBCheckType::Full, &db_conn)
                .await
                .expect("Check shouldn't fail");
            master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            let assignation_result: OeilTDResult = master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(
                assignation_result.updated(),
                &1, // No imported into the BL, so overwritting the BLU
                "it should've updated 1 task"
            );
            assert_eq!(assignation_result.added(), &0, "it should've added 0 task");
            assert_eq!(
                assignation_result.removed(),
                &0,
                "it should've removed 0 task"
            );
            let task_assignation: Option<Uuid> = check_assigned(
                &master_oeil
                    .get_assigned_oeil(&constraint_id)
                    .await
                    .expect("should get the task assignation")
                    .unwrap(),
            );
            assert_eq!(task_assignation.is_some(), true, "Should be assigned");
            assert_eq!(
                task_assignation.unwrap(),
                *master_oeil.visage().id(),
                "Oeil id mismatch"
            );
            let exists: bool = redis::Cmd::sismember(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(master_oeil.visage().id()),
                constraint_id.to_string(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .expect("should check if record exists in set");
            assert_eq!(exists, true, "It should exists in the BLU");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn done_task_on_active_oeil() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(1)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            master_oeil
                .master_set_time_check(
                    oeil::YEUX_INACTIVE_UPDATE_TS,
                    Utc::now() - Duration::seconds(30),
                )
                .await
                .expect("the timestamps should've been set");
            let score_before: usize =
                redis::Cmd::zincr(YEUX, master_oeil.visage().id().to_string(), 1 as i64)
                    .query_async(&mut oeil_test.test_conn)
                    .await
                    .expect("to increment the oeil score in redis");
            master_oeil
                .set_assigned_oeil(
                    &constraint_id,
                    OeilTaskStatus::Active(*master_oeil.visage().id()),
                )
                .await
                .expect("set the assigned status");
            let _res: usize = redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(*master_oeil.visage().id()),
                constraint_id.to_string(),
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .expect("to add to the update list");
            let res = master_oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::New)
                .await
                .expect("To have imported the new job");
            assert_eq!(*res.added(), 1, "There should be 1 added job");
            master_oeil
                .master_db_check_internal(OeilDBCheckType::Inactive, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let assigned_oeil: Option<Uuid> = check_assigned(
                &master_oeil
                    .get_assigned_oeil(&constraint_id)
                    .await
                    .expect("should get the assigned status of the task")
                    .unwrap(),
            );
            assert_eq!(
                assigned_oeil.is_some(),
                true,
                "The task should be assigned yet"
            );
            assert_eq!(
                assigned_oeil.unwrap(),
                *master_oeil.visage().id(),
                "The task should be assigned yet"
            );
            let assigned_oeil: OeilTaskStatus = master_oeil
                .get_assigned_oeil(&constraint_id)
                .await
                .expect("should get the assigned status of the task")
                .unwrap();
            match assigned_oeil {
                OeilTaskStatus::Done(id) => assert_eq!(id, *master_oeil.visage().id()),
                _ => panic!("Should be 'done'"),
            };
            let assignation_result: OeilTDResult = master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(
                assignation_result.updated(),
                &0,
                "it should've updated 1 task"
            );
            let score_after: usize =
                redis::Cmd::zscore(YEUX, master_oeil.visage().id().to_string())
                    .query_async(&mut oeil_test.test_conn)
                    .await
                    .expect("to increment the oeil score in redis");
            if !(score_after < score_before) {
                panic!(
                    "The score should have decreased {} < {}",
                    score_after, score_before
                );
            }
            let res = master_oeil
                .slave_check_loop_internal(&db_conn, OeilSlaveCheckType::Done)
                .await
                .expect("To have imported the new job");
            assert_eq!(*res.removed(), 1, "Should have removed 1 job");
            let exists: (bool, bool, bool, bool) = redis::pipe()
                .hexists(YEUX_BACKLOG, constraint_id.to_string())
                .sismember(
                    YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(master_oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .hexists(
                    YEUX_OEIL_BACKLOG_FORMAT!(master_oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .sismember(
                    YEUX_OEIL_BACKLOG_DONE_FORMAT!(master_oeil.visage().id()),
                    constraint_id.to_string(),
                )
                .query_async(&mut oeil_test.test_conn)
                .await
                .expect("should check if record exists in set");
            assert_eq!(exists.0, false, "Shouldn't exists in the YBL");
            assert_eq!(exists.1, false, "Shouldn't exists in the oeil BLU");
            assert_eq!(exists.2, false, "Shouldn't exists in the oeil BL");
            assert_eq!(exists.3, false, "Shouldn't exists in the the oeil BLD");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn done_task_on_inactive_oeil() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(1)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            master_oeil
                .master_set_time_check(
                    oeil::YEUX_INACTIVE_UPDATE_TS,
                    Utc::now() - Duration::seconds(30),
                )
                .await
                .expect("the timestamps should've been set");
            master_oeil
                .set_assigned_oeil(&constraint_id, OeilTaskStatus::Inactive)
                .await
                .expect("set the assigned status");
            master_oeil
                .master_db_check_internal(OeilDBCheckType::Inactive, &db_conn)
                .await
                .expect("Check shouldn't fail");
            let assigned_oeil: Option<OeilTaskStatus> = master_oeil
                .get_assigned_oeil(&constraint_id)
                .await
                .expect("should get the assigned status of the task");
            assert_eq!(
                assigned_oeil.is_none(),
                true,
                "The task should be assigned and should'nt exists anymore"
            );
            let assignation_result: OeilTDResult = master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(
                assignation_result.updated(),
                &0,
                "it should've updated 1 task"
            );
            let exists: bool = redis::Cmd::hexists(YEUX_BACKLOG, constraint_id.to_string())
                .query_async(&mut oeil_test.test_conn)
                .await
                .expect("should check if record exists in set");
            assert_eq!(exists, false, "Shouldn't exists in the YBL");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn corrupted_status_data() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(1)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            let _a: usize = redis::Cmd::hset(
                YEUX_BACKLOG,
                constraint_id.to_string(),
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .expect("to set the status of the task");
            let assignation_result: OeilTDResult = master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(
                assignation_result.updated(),
                &1,
                "it should've updated 1 task"
            );
            let record: Option<OeilTaskStatus> = master_oeil
                .get_assigned_oeil(&constraint_id)
                .await
                .expect("getting the task status");
            assert_eq!(record.is_some(), true, "Should still be in the YBL");
            match record.unwrap() {
                OeilTaskStatus::Inactive => {
                    // Good
                }
                _ => panic!("Should've been inactive"),
            }
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}

#[test]
fn corrupted_key_data() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut oeil_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
                Some(Utc::now() - Duration::seconds(1)),
                None,
            );
            oeil_test.config.visage.master.enable = false;
            oeil_test.config.visage.slave.enable = true;
            oeil_test.config.visage.master.enable = true;
            oeil_test.config.visage.rates.ttl_lease = 60;
            let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
                .await
                .expect("the oeil should've been built correctly");
            master_oeil
                .visage_mut()
                .register_lease()
                .await
                .expect("to register the oeil");
            let _a: usize = redis::Cmd::hset(
                YEUX_BACKLOG,
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            )
            .query_async(&mut oeil_test.test_conn)
            .await
            .expect("to set the status of the task");
            let assignation_result: OeilTDResult = master_oeil
                .master_distribute_tasks()
                .await
                .expect("the task distribution should'nt fail");
            assert_eq!(
                assignation_result.updated(),
                &0,
                "it should'nt have updated a task"
            );
            let record: Option<OeilTaskStatus> = master_oeil
                .get_assigned_oeil(&constraint_id)
                .await
                .expect("getting the task status");
            assert_eq!(record.is_none(), true, "Shouldn't still be in the YBL");
            clean_up(&mut oeil_test).await;
            Ok(())
        })
    });
}
