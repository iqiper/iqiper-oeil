use super::oeil::Oeil;
use super::*;

#[tokio::test]
async fn no_yeux() {
    let mut oeil_test = setup().await;
    oeil_test.config.visage.master.enable = true;
    oeil_test.config.visage.rates.ttl_lease = 60;
    let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
        .await
        .expect("The oeil should've been created");
    master_oeil
        .visage_mut()
        .register_lease()
        .await
        .expect("should be registered");
    oeil_test.config.visage.master.enable = false;
    oeil_test.config.visage.slave.enable = true;
    master_oeil
        .master_check_orphaned_oeil()
        .await
        .expect("Cleaning orphaned oeil failed");
    let res: (bool, Option<usize>) = redis::pipe()
        .exists(visage::VISAGE_ID_FORMAT!(master_oeil.visage().id()))
        .zscore(oeil::YEUX, master_oeil.visage().id().to_string())
        .query_async(&mut oeil_test.test_conn)
        .await
        .expect("The verification in redis to be fetched");
    assert_eq!(res.0, true, "The witness key should exists");
    assert_eq!(
        res.1.is_some(),
        true,
        "The oeil should exist in the the YEUX sset"
    );
    clean_up(&mut oeil_test).await;
}

#[tokio::test]
async fn expired_oeil() {
    let mut oeil_test = setup().await;
    oeil_test.config.visage.master.enable = true;
    oeil_test.config.visage.rates.ttl_lease = 60;
    let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
        .await
        .expect("The oeil should've been created");
    master_oeil
        .visage_mut()
        .register_lease()
        .await
        .expect("should be registered");
    oeil_test.config.visage.master.enable = false;
    oeil_test.config.visage.slave.enable = true;
    oeil_test.config.visage.rates.ttl_lease = 1;
    let mut slave_oeil = vec![
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
    ];
    for oeil in slave_oeil.iter_mut() {
        oeil.visage_mut()
            .register_lease()
            .await
            .expect("to register the slave oeil");
    }
    std::thread::sleep(std::time::Duration::from_secs(1));
    master_oeil
        .master_check_orphaned_oeil()
        .await
        .expect("Cleaning orphaned oeil failed");
    let res: (
        bool,
        bool,
        bool,
        Option<usize>,
        Option<usize>,
        Option<usize>,
    ) = redis::pipe()
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[0].visage().id()))
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[1].visage().id()))
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[2].visage().id()))
        .zscore(oeil::YEUX, slave_oeil[0].visage().id().to_string())
        .zscore(oeil::YEUX, slave_oeil[1].visage().id().to_string())
        .zscore(oeil::YEUX, slave_oeil[2].visage().id().to_string())
        .query_async(&mut oeil_test.test_conn)
        .await
        .expect("The verification in redis to be fetched");
    assert_eq!(res.0, false, "The witness key should exists");
    assert_eq!(res.1, false, "The witness key should exists");
    assert_eq!(res.2, false, "The witness key should exists");
    assert_eq!(
        res.3.is_some(),
        false,
        "The oeil should exist in the the YEUX sset"
    );
    assert_eq!(
        res.4.is_some(),
        false,
        "The oeil should exist in the the YEUX sset"
    );
    assert_eq!(
        res.5.is_some(),
        false,
        "The oeil should exist in the the YEUX sset"
    );
    clean_up(&mut oeil_test).await;
}

#[tokio::test]
async fn not_expired_oeil() {
    let mut oeil_test = setup().await;
    oeil_test.config.visage.master.enable = true;
    oeil_test.config.visage.rates.ttl_lease = 60;
    let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
        .await
        .expect("The oeil should've been created");
    master_oeil
        .visage_mut()
        .register_lease()
        .await
        .expect("should be registered");
    oeil_test.config.visage.master.enable = false;
    oeil_test.config.visage.slave.enable = true;
    oeil_test.config.visage.rates.ttl_lease = 10;
    let mut slave_oeil = vec![
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
    ];
    for oeil in slave_oeil.iter_mut() {
        oeil.visage_mut()
            .register_lease()
            .await
            .expect("to register the slave oeil");
    }
    master_oeil
        .master_check_orphaned_oeil()
        .await
        .expect("Cleaning orphaned oeil failed");
    let res: (
        bool,
        bool,
        bool,
        Option<usize>,
        Option<usize>,
        Option<usize>,
    ) = redis::pipe()
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[0].visage().id()))
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[1].visage().id()))
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[2].visage().id()))
        .zscore(oeil::YEUX, slave_oeil[0].visage().id().to_string())
        .zscore(oeil::YEUX, slave_oeil[1].visage().id().to_string())
        .zscore(oeil::YEUX, slave_oeil[2].visage().id().to_string())
        .query_async(&mut oeil_test.test_conn)
        .await
        .expect("The verification in redis to be fetched");
    assert_eq!(res.0, true, "The witness key should exists");
    assert_eq!(res.1, true, "The witness key should exists");
    assert_eq!(res.2, true, "The witness key should exists");
    assert_eq!(
        res.3.is_some(),
        true,
        "The oeil should exist in the the YEUX sset"
    );
    assert_eq!(
        res.4.is_some(),
        true,
        "The oeil should exist in the the YEUX sset"
    );
    assert_eq!(
        res.5.is_some(),
        true,
        "The oeil should exist in the the YEUX sset"
    );
    clean_up(&mut oeil_test).await;
}

#[tokio::test]
async fn unknown_key() {
    let mut oeil_test = setup().await;
    oeil_test.config.visage.master.enable = true;
    oeil_test.config.visage.rates.ttl_lease = 60;
    let mut master_oeil = Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
        .await
        .expect("The oeil should've been created");
    master_oeil
        .visage_mut()
        .register_lease()
        .await
        .expect("should be registered");
    oeil_test.config.visage.master.enable = false;
    oeil_test.config.visage.slave.enable = true;
    oeil_test.config.visage.rates.ttl_lease = 10;
    let mut slave_oeil = vec![
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
        Oeil::new(oeil_test.config.clone(), OEIL_METRICS.deref().clone())
            .await
            .expect("The oeil should've been created"),
    ];
    for oeil in slave_oeil.iter_mut() {
        oeil.visage_mut()
            .register_lease()
            .await
            .expect("to register the slave oeil");
    }
    let add_dummy: bool = redis::Cmd::zadd(oeil::YEUX, "AAAAAAAAAAAAAAAAAAAAA", 0)
        .query_async(&mut oeil_test.test_conn)
        .await
        .expect("Failed to add dummy value to redis");
    assert_eq!(add_dummy, true, "Adding dummy value failed");
    master_oeil
        .master_check_orphaned_oeil()
        .await
        .expect("Cleaning orphaned oeil failed");
    let res: (
        bool,
        bool,
        bool,
        Option<usize>,
        Option<usize>,
        Option<usize>,
    ) = redis::pipe()
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[0].visage().id()))
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[1].visage().id()))
        .exists(visage::VISAGE_ID_FORMAT!(slave_oeil[2].visage().id()))
        .zscore(oeil::YEUX, slave_oeil[0].visage().id().to_string())
        .zscore(oeil::YEUX, slave_oeil[1].visage().id().to_string())
        .zscore(oeil::YEUX, slave_oeil[2].visage().id().to_string())
        .query_async(&mut oeil_test.test_conn)
        .await
        .expect("The verification in redis to be fetched");
    assert_eq!(res.0, true, "The witness key should exists");
    assert_eq!(res.1, true, "The witness key should exists");
    assert_eq!(res.2, true, "The witness key should exists");
    assert_eq!(
        res.3.is_some(),
        true,
        "The oeil should exist in the the YEUX sset"
    );
    assert_eq!(
        res.4.is_some(),
        true,
        "The oeil should exist in the the YEUX sset"
    );
    assert_eq!(
        res.5.is_some(),
        true,
        "The oeil should exist in the the YEUX sset"
    );
    clean_up(&mut oeil_test).await;
}
