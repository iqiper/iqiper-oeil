use std::{fmt, io};
use visage::errors::VisageError;

/// Wrap all kind of error that could be encoutered by the service.
#[derive(Debug)]
pub enum InternalError {
    /// Wrap an IO error with the name of the file or output linked to the error.
    IO(String, io::Error),
    /// Error occuring when parsing the configuration
    ConfigError(config::ConfigError),
    /// Error occuring when redis fails in some way
    RedisError(redis::RedisError),
    /// Error occuring when building database connection pool
    R2D2Error(r2d2::Error),
    /// Error occuring when trying to parse a faulty uuid
    UuidError(uuid::Error),
    /// Error occuring when a database function fails
    DBError(db::error::IqiperDatabaseError),
    /// Error when connecting a simple database connection
    DieselConnectionError(diesel::ConnectionError),
    // Occuring when failing to serialize or deserialize using serde json
    SerdeJsonErr(serde_json::Error),
    // Error from 'visage'
    VisageError(VisageError),
    // Error from 'peau'
    PeauError(peau::errors::PeauError),
    // Error from Prometheus
    PrometheusError(peau::prometheus::Error),
}

impl fmt::Display for InternalError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            InternalError::IO(filename, io_err) => write!(f, "{}: {}", filename, io_err),
            InternalError::RedisError(err) => write!(f, "RedisError : {}", err),
            InternalError::R2D2Error(err) => write!(f, "R2D2Error : {}", err),
            InternalError::UuidError(err) => write!(f, "UuidError : {}", err),
            InternalError::ConfigError(err) => write!(f, "ConfigError : {}", err),
            InternalError::SerdeJsonErr(err) => write!(f, "SerdeJsonErr : {}", err),
            InternalError::PrometheusError(err) => write!(f, "PrometheusError : {}", err),
            InternalError::PeauError(err) => write!(f, "PeauError : {}", err),
            InternalError::VisageError(err) => write!(f, "VisageError : {}", err),
            InternalError::DieselConnectionError(err) => {
                write!(f, "DieselConnectionError : {}", err)
            }
            InternalError::DBError(err) => write!(f, "DBError : {}", err),
        }
    }
}

impl std::error::Error for InternalError {}

impl From<config::ConfigError> for InternalError {
    fn from(err: config::ConfigError) -> Self {
        InternalError::ConfigError(err)
    }
}

impl From<redis::RedisError> for InternalError {
    fn from(err: redis::RedisError) -> Self {
        InternalError::RedisError(err)
    }
}

impl From<r2d2::Error> for InternalError {
    fn from(err: r2d2::Error) -> Self {
        InternalError::R2D2Error(err)
    }
}

impl From<uuid::Error> for InternalError {
    fn from(err: uuid::Error) -> Self {
        InternalError::UuidError(err)
    }
}

impl From<db::error::IqiperDatabaseError> for InternalError {
    fn from(err: db::error::IqiperDatabaseError) -> Self {
        InternalError::DBError(err)
    }
}

impl From<diesel::ConnectionError> for InternalError {
    fn from(err: diesel::ConnectionError) -> Self {
        InternalError::DieselConnectionError(err)
    }
}

impl From<serde_json::Error> for InternalError {
    fn from(err: serde_json::Error) -> Self {
        InternalError::SerdeJsonErr(err)
    }
}

impl From<std::io::Error> for InternalError {
    fn from(err: std::io::Error) -> Self {
        InternalError::IO(String::from("unknown"), err)
    }
}

impl From<VisageError> for InternalError {
    fn from(err: VisageError) -> Self {
        InternalError::VisageError(err)
    }
}

impl From<peau::errors::PeauError> for InternalError {
    fn from(err: peau::errors::PeauError) -> Self {
        InternalError::PeauError(err)
    }
}

impl From<peau::prometheus::Error> for InternalError {
    fn from(err: peau::prometheus::Error) -> Self {
        InternalError::PrometheusError(err)
    }
}
