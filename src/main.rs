//! `iqiper-oeil` is a service looking over constraints for `iqiper`
//!
//! The service use the `visage` crate to get a lease in redis.
//!
//! ## The master role
//!
//! The master will check for new/to-update/to-delete tasks in the database.
//! It'll then distribute those tasks to the slave instance.
//!
//! ## The slave role
//!
//! The slave will check for new/to-update tasks in its backlog (list)
//! It'll download from the database the details concerning a task to run.
//! After that, it'll check for tasks to delete in its other backlog (list).
//! Finally it'll check every tasks depending on type of constraint. If a tasks
//! require some notification/alert, it'll create the communication object and then
//! add it to the `bouche` backlog.

#[allow(unused_imports)]
extern crate openssl;
mod configuration;
mod errors;
mod oeil;
#[cfg(test)]
mod tests;
use chrono::{DateTime, Duration, Utc};
use errors::InternalError;
use log::{debug, info, warn};
use oeil::Oeil;
use peau::actix_web;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
const CONF_FILE: &str = "IQIPER_OEIL_CONFIG";
const ENV_CONF_VAR: &str = "IQIPER_OEIL";

macro_rules! PERSIST_OEIL {
    ($oeil:expr, $start_date:expr) => {
        let now = Utc::now();
        if now >= $start_date + Duration::milliseconds($oeil.visage().config().rates.tick_rate) {
            warn!("The oeil is running slow ! {} ms late", now - $start_date);
            $oeil.visage().update_lease().await?;
        }
    };
}

/// Initialize the master oeil
///
/// # Arguments
/// * `oeil` - The oeil object registered in `redis`
///
/// # Return Value
/// Nothing
async fn master_init(oeil: &Oeil) -> Result<(), InternalError> {
    oeil.master_check_orphaned_oeil().await?;
    oeil.master_db_check(oeil::OeilDBCheckType::Full).await?;
    Ok(())
}

/// Loop to execute when running as master.
/// It does the following :
///
/// 1. Check for updated jobs in the database
/// 2. Check for inactive jobs in the database
/// 3. Check for newly active jobs in the database
/// 4. Distribute tasks to the active slave
///
/// Between each points of the list above it refreshes
/// registration of the running `oeil`
///
/// # Arguments
/// * `oeil` - The oeil object registered in `redis`
/// * `start_date` - The at which the loop had started.
///
/// # Return Value
/// Nothing
async fn master_loop(oeil: &Oeil, start_date: &DateTime<Utc>) -> Result<(), InternalError> {
    oeil.master_db_check(oeil::OeilDBCheckType::Updated).await?;
    PERSIST_OEIL!(oeil, *start_date);
    oeil.master_db_check(oeil::OeilDBCheckType::Inactive)
        .await?;
    PERSIST_OEIL!(oeil, *start_date);
    oeil.master_db_check(oeil::OeilDBCheckType::Active).await?;
    PERSIST_OEIL!(oeil, *start_date);
    oeil.master_distribute_tasks().await?;
    PERSIST_OEIL!(oeil, *start_date);
    Ok(())
}

/// Loop to execute when running as slave.
/// It does the following :
///
/// 1. Check for new tasks
/// 2. Run the tasks
/// 3. Check for done tasks
///
/// Between each points of the list above it refreshes
/// registration of the running `oeil`
///
/// # Arguments
/// * `oeil` - The oeil object registered in `redis`
/// * `start_date` - The at which the loop had started.
///
/// # Return Value
/// Nothing
async fn slave_loop(oeil: &Oeil, start_date: &DateTime<Utc>) -> Result<(), InternalError> {
    oeil.slave_check_loop(oeil::OeilSlaveCheckType::New).await?;
    PERSIST_OEIL!(oeil, *start_date);
    oeil.slave_run_tasks().await?;
    PERSIST_OEIL!(oeil, *start_date);
    oeil.slave_check_loop(oeil::OeilSlaveCheckType::Done)
        .await?;
    PERSIST_OEIL!(oeil, *start_date);
    Ok(())
}

/// Main loop of the program.
/// It does the following :
///
/// 1. Update the lease
/// 2. Run the master loop if the `oeil` is a master
/// 3. Run the slave loop if the `oeil` is a slave
/// 4. Sleep if the loop has run faster than the tick rate.
///
/// # Arguments
/// * `oeil` - The oeil object registered in `redis`
/// * `term_signal` - The atomic bool set to true went a stop signal is encountered
///
/// # Return Value
/// Nothing
async fn main_loop(oeil: &Oeil, term_signal: Arc<AtomicBool>) -> Result<(), InternalError> {
    let tick_rate = Duration::milliseconds(oeil.visage().config().rates.tick_rate);
    if oeil.visage().config().master.enable {
        master_init(oeil).await?;
    }
    while !term_signal.load(Ordering::Relaxed) {
        debug!("TICK!");
        oeil.visage().update_lease().await?;
        let start_date = Utc::now();
        if oeil.visage().config().master.enable {
            master_loop(oeil, &start_date).await?;
        }
        if oeil.visage().config().slave.enable {
            slave_loop(oeil, &start_date).await?;
        }
        let end_date = Utc::now();
        let diff_date = end_date - start_date;
        oeil.metrics().saturation().set(
            (tick_rate.num_milliseconds() - diff_date.num_milliseconds())
                / tick_rate.num_milliseconds(),
        );
        if diff_date < tick_rate {
            std::thread::sleep(
                (tick_rate - diff_date)
                    .to_std()
                    .unwrap_or_else(|_e| std::time::Duration::from_secs(0)),
            );
        }
    }
    Ok(())
}

/// Handle the signals
///
/// # Arguments
/// * `term_signal` - The atomic bool set to true went a stop signal is encountered
///
/// # Return Value
/// Nothing
fn handle_signals(term_signal: Arc<AtomicBool>) -> Result<(), InternalError> {
    signal_hook::flag::register(signal_hook::SIGINT, Arc::clone(&term_signal))?;
    signal_hook::flag::register(signal_hook::SIGTERM, Arc::clone(&term_signal))?;
    unsafe {
        signal_hook::register(signal_hook::SIGINT, || {
            warn!("Received SIGINT. Will shutdown at the end of this tick...");
        })?;
        signal_hook::register(signal_hook::SIGTERM, || {
            warn!("Received SIGTERM. Will shutdown at the end of this tick...");
        })?;
    }
    Ok(())
}

#[macro_export]
macro_rules! iqiper_oeil_app {
    ($conf: expr, $redis_comm: expr, $metrics: expr) => {
        actix_web::App::new()
            .wrap(peau::actix_web::middleware::Logger::default())
            .wrap($metrics)
            .app_data(
                peau::actix_web::web::PathConfig::default()
                    .error_handler(|err, _| peau::actix_http::error::ErrorBadRequest(err)),
            )
            .app_data(
                peau::actix_web::web::QueryConfig::default()
                    .error_handler(|err, _| peau::actix_http::error::ErrorBadRequest(err)),
            )
            .configure(peau::web::health::alive)
            .configure(peau::web::health::ready)
    };
}

#[tokio::main]
async fn main() -> Result<(), InternalError> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug")).init();
    let term_signal = Arc::new(AtomicBool::new(false));
    let local_server_set = tokio::task::LocalSet::new();
    let _server_sys = actix_web::rt::System::run_in_tokio("actix_server", &local_server_set);
    handle_signals(term_signal.clone())?;
    info!("Reading configuration...");
    let config = configuration::get(
        std::path::Path::new(
            std::env::var(CONF_FILE)
                .unwrap_or("./config.yml".to_string())
                .as_str(),
        ),
        ENV_CONF_VAR,
    )?;
    let mut labels = std::collections::HashMap::new();
    labels.insert(
        "oeil_role".to_string(),
        visage::Visage::get_role(&config.visage).to_string(),
    );
    let oeil_metrics = oeil::OeilMetrics::new()?;
    let mut oeil = Oeil::new(config.clone(), oeil_metrics.clone()).await?;
    labels.insert("oeil_id".to_string(), oeil.visage().id().to_string());
    let shared_registry =
        peau::prometheus::Registry::new_custom(Some(String::from("oeil")), Some(labels)).unwrap();
    oeil_metrics.register(&shared_registry)?;
    let metrics_http = peau::actix_web_prom::PrometheusMetrics::new_with_registry(
        shared_registry,
        "actix",
        Some("/-/metrics"),
        None,
    )
    .unwrap();
    let server = actix_web::HttpServer::new(move || {
        iqiper_oeil_app!(config, redis_comm, metrics_http.clone())
    });
    let _server_control = peau::web::web::bind_web_server(&config.server, server)?.run();
    oeil.visage_mut().register_lease().await?;
    peau::web::health::set_ready();
    peau::web::health::set_alive(true);
    info!("Starting oeil {}", oeil.visage().id());
    main_loop(&oeil, term_signal).await?;
    peau::web::health::set_alive(false);
    actix_web::rt::System::current().stop(); //TODO Find a cleaner way to shutdown actix
    oeil.visage().unregister_lease(None).await?;
    Ok(())
}
