use super::*;
use diesel::Connection;

impl Oeil {
    /// Check on which `oeil` is a task assigned, if on any
    ///
    /// # Arguments
    /// * `task_id` - The id of the task
    ///
    /// # Return Value
    /// The task if any
    pub(crate) async fn get_assigned_oeil(
        &self,
        task_id: &Uuid,
    ) -> Result<Option<OeilTaskStatus>, InternalError> {
        let record: Option<String> = self
            .visage()
            .apply_cmd_in_redis(&mut redis::Cmd::hget(YEUX_BACKLOG, task_id.to_string()))
            .await?;
        match record {
            Some(x) => match serde_json::from_str::<OeilTask>(x.as_str()) {
                Ok(task) => Ok(Some(task.status().clone())),
                Err(_e) => {
                    self.set_assigned_oeil(task_id, OeilTaskStatus::Inactive)
                        .await?;
                    Ok(None)
                }
            },
            None => Ok(None),
        }
    }

    /// Assign a task to an `oeil`
    ///
    /// # Arguments
    /// * `task_id` - The id of the task
    /// * `status` - The status to set the task to
    ///
    /// # Return Value
    /// The command to execute
    pub(super) fn set_assigned_oeil_internal(
        &self,
        task_id: &Uuid,
        status: OeilTaskStatus,
    ) -> Result<redis::Cmd, InternalError> {
        Ok(redis::Cmd::hset(
            YEUX_BACKLOG,
            task_id.to_string(),
            serde_json::to_string(&OeilTask::new(status))?,
        ))
    }

    /// Assign a task to an `oeil`
    ///
    /// # Arguments
    /// * `task_id` - The id of the task
    /// * `status` - The status to set the task to
    ///
    /// # Return Value
    /// Nothing
    pub(crate) async fn set_assigned_oeil(
        &self,
        task_id: &Uuid,
        status: OeilTaskStatus,
    ) -> Result<(), InternalError> {
        self.visage()
            .apply_cmd_in_redis(&mut self.set_assigned_oeil_internal(task_id, status)?)
            .await?;
        Ok(())
    }

    /// Check if a task is assigned.
    ///
    /// # Arguments
    /// * `oeil_id` - The id of the `oeil`
    /// * `task_id` - The id of the task
    ///
    /// # Return Value
    /// True if assigned
    pub(crate) async fn check_if_assigned(
        &self,
        oeil_id: &Uuid,
        task_id: &Uuid,
    ) -> Result<bool, InternalError> {
        let res: (bool, bool) = self
            .visage()
            .apply_pipe_in_redis(
                &mut redis::pipe()
                    .hexists(YEUX_OEIL_BACKLOG_FORMAT!(oeil_id), task_id.to_string())
                    .sismember(
                        YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil_id),
                        task_id.to_string(),
                    ),
            )
            .await?;
        Ok(res.0 || res.1)
    }

    /// Add a task to the `update` list of a slave
    ///
    /// # Arguments
    /// * `oeil_id` - The id of the `oeil`
    /// * `task_id` - The id of the task
    ///
    /// # Return Value
    /// Nothing
    pub(crate) async fn add_to_slave_update_bl(
        &self,
        oeil_id: &Uuid,
        task_id: &Uuid,
    ) -> Result<(), InternalError> {
        self.visage()
            .apply_cmd_in_redis(&mut redis::Cmd::sadd(
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil_id),
                task_id.to_string(),
            ))
            .await?;
        Ok(())
    }

    /// When running as master, this allows to get last time a kind of check was made
    ///
    /// # Arguments
    /// * `fallback` - The date to return if None is returned
    /// * `keys` - A list of keys to check. If one doesn't exists, try the next one and so on.
    /// If none exists the fallback is returned
    ///
    /// # Return Value
    /// A time to consider the value at which the last check was made
    pub(super) async fn master_get_time_check(
        &self,
        fallback: &DateTime<Utc>,
        keys: &mut Vec<&str>,
    ) -> Result<DateTime<Utc>, InternalError> {
        let mut res: Option<DateTime<Utc>> = None;

        while keys.len() > 0 && res.is_none() {
            let date_in_redis: Option<String> = self
                .visage()
                .apply_cmd_in_redis(&mut redis::Cmd::get(keys.remove(0)))
                .await?;
            res = match date_in_redis {
                Some(x) => {
                    let parsed =
                        DateTime::parse_from_rfc3339(x.as_str()).map(|x| x.with_timezone(&Utc));
                    match parsed {
                        Ok(x) => Some(x),
                        Err(_e) => None,
                    }
                }
                None => None,
            };
        }
        Ok(res.unwrap_or_else(|| *fallback))
    }

    /// When running as master, this allows to set a key to the date the check was done.
    ///
    /// # Arguments
    /// * `key` - The key to set
    /// * `time` - The time to set
    ///
    /// # Return Value
    /// Nothing
    pub(crate) async fn master_set_time_check(
        &self,
        key: &str,
        time: DateTime<Utc>,
    ) -> Result<(), InternalError> {
        Ok(self
            .visage()
            .apply_cmd_in_redis(&mut redis::Cmd::set(
                key,
                time.to_rfc3339_opts(chrono::SecondsFormat::Micros, true),
            ))
            .await?)
    }

    /// Add a communication to the backlog of `bouche` for processing
    ///
    /// # Arguments
    /// * `conn` - The connection to the database to use
    /// * `task_id` - The id of the task to add to the bouche backlog
    /// * `reason` - The reason this communication is beeing sent out
    ///
    /// # Return Value
    /// The redis command to execute
    pub(crate) fn add_to_bouche_bl(
        &self,
        conn: &diesel::PgConnection,
        task_id: &Uuid,
        reason: CommunicationReasonType,
    ) -> Result<redis::Cmd, InternalError> {
        let communication_id: Uuid =
            conn.transaction::<Uuid, db::error::IqiperDatabaseError, _>(|| {
                let constraint = db::activity_constraint::select_by_unique_id(&conn, &task_id)?;
                let uid = db::activity_constraint::select_uid(&conn, &task_id)?;
                let res = db::communication::create_from_constraint(
                    &conn,
                    &task_id,
                    db::communication::NewCommunication {
                        uid,
                        type_: db::enums::CommunicationType::Email,
                        reason,
                    },
                )?;
                db::activity::end_if_done(&conn, &uid, &constraint.activity_id)?;
                Ok(res.id)
            })?;
        Ok(redis::Cmd::lpush(
            self.config().bouche_backlog.as_str(),
            communication_id.to_string(),
        ))
    }
}
