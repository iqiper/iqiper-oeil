use super::*;
use peau::prometheus::{Histogram, HistogramOpts, IntCounter, IntGauge, Registry};

#[derive(Debug, Clone, Copy, Display)]
pub enum OeilDBCheckType {
    Full,
    Inactive,
    Active,
    Updated,
}

#[derive(Debug, Clone, Copy, Display)]
pub enum OeilSlaveCheckType {
    New,
    Done,
}

#[derive(Getters, Setters, MutGetters, Deserialize, Serialize)]
pub struct OeilConstraint {
    #[getset(get = "pub")]
    constraint: db::activity_constraint::SelectActivityConstraint,
    #[getset(get = "pub", set = "pub")]
    notified: bool,
}

impl OeilConstraint {
    pub fn new(constraint: db::activity_constraint::SelectActivityConstraint) -> Self {
        OeilConstraint {
            constraint,
            notified: false,
        }
    }
}

#[derive(Getters, MutGetters)]
pub struct Oeil {
    #[getset(get = "pub", get_mut = "pub")]
    visage: Visage,
    #[getset(get = "pub")]
    config: crate::configuration::ConfigurationOeil,
    #[getset(get = "pub")]
    metrics: OeilMetrics,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum OeilTaskStatus {
    Inactive,
    Active(Uuid),
    Done(Uuid),
}

#[derive(Getters, Clone, Debug, Serialize, Deserialize)]
pub struct OeilTask {
    #[getset(get = "pub")]
    status: OeilTaskStatus,
}

#[derive(Getters, Clone, Debug, Serialize, Deserialize)]
pub struct OeilTDResult {
    #[getset(get = "pub")]
    added: usize,
    #[getset(get = "pub")]
    updated: usize,
    #[getset(get = "pub")]
    removed: usize,
}

#[derive(Getters, Clone, Debug, Serialize, Deserialize)]
pub struct OeilTaskResult {
    #[getset(get = "pub")]
    success: usize,
    #[getset(get = "pub")]
    failed: usize,
    #[getset(get = "pub")]
    skipped: usize,
    #[getset(get = "pub")]
    notified: usize,
    #[getset(get = "pub")]
    badly_formatted: usize,
}

#[derive(Getters, Clone, Debug)]
pub struct OeilMetrics {
    #[getset(get = "pub")]
    saturation: IntGauge,
    #[getset(get = "pub")]
    run_tasks_hist: Histogram,
    #[getset(get = "pub")]
    fetch_hist: Histogram,
    #[getset(get = "pub")]
    done_hist: Histogram,
    #[getset(get = "pub")]
    orphaned_hist: Histogram,
    #[getset(get = "pub")]
    distributed_hist: Histogram,
    #[getset(get = "pub")]
    distributed_nb: IntCounter,
    #[getset(get = "pub")]
    nb_tasks_checked: IntGauge,
    #[getset(get = "pub")]
    db_check_active: Histogram,
    #[getset(get = "pub")]
    db_check_updated: Histogram,
    #[getset(get = "pub")]
    db_check_done: Histogram,
    #[getset(get = "pub")]
    nb_tasks: IntGauge,
    #[getset(get = "pub")]
    nb_tasks_success: IntGauge,
    #[getset(get = "pub")]
    nb_tasks_failure: IntCounter,
    #[getset(get = "pub")]
    nb_tasks_skipped: IntCounter,
    #[getset(get = "pub")]
    nb_fetched_task: IntCounter,
    #[getset(get = "pub")]
    nb_done_task: IntCounter,
    #[getset(get = "pub")]
    nb_bouche_job: IntCounter,
}

impl OeilMetrics {
    pub fn new() -> Result<Self, InternalError> {
        Ok(OeilMetrics {
            saturation: IntGauge::new(
                "saturation",
                "The time running every function in the main loop takes",
            )?,
            run_tasks_hist: Histogram::with_opts(HistogramOpts::new(
                "task_latencies",
                "The time running every checks takes",
            ))?,
            fetch_hist: Histogram::with_opts(HistogramOpts::new(
                "task_fetch",
                "The time it takes to fetch tasks",
            ))?,
            done_hist: Histogram::with_opts(HistogramOpts::new(
                "task_done",
                "The time it takes to fetch done tasks",
            ))?,
            orphaned_hist: Histogram::with_opts(HistogramOpts::new(
                "orphaned_hist",
                "The time it takes to check if every oeil is alive",
            ))?,
            distributed_hist: Histogram::with_opts(HistogramOpts::new(
                "distributed_hist",
                "The time it takes to distribute the tasks to the slave",
            ))?,
            db_check_active: Histogram::with_opts(HistogramOpts::new(
                "db_check_active",
                "The time it takes to fetch newly active tasks from db",
            ))?,
            db_check_updated: Histogram::with_opts(HistogramOpts::new(
                "db_check_updated",
                "The time it takes to fetch newly updated tasks from db",
            ))?,
            db_check_done: Histogram::with_opts(HistogramOpts::new(
                "db_check_done",
                "The time it takes to fetch newly done tasks from db",
            ))?,
            nb_tasks: IntGauge::new("nb_tasks", "The number of tasks")?,
            nb_tasks_success: IntGauge::new(
                "nb_tasks_success",
                "The number of successful tasks, during this loop.",
            )?,
            nb_tasks_failure: IntCounter::new(
                "nb_tasks_failure",
                "The number of failing tasks, during this loop.",
            )?,
            nb_tasks_skipped: IntCounter::new(
                "nb_tasks_skipped",
                "The number of skipped tasks, during this loop.",
            )?,
            nb_fetched_task: IntCounter::new(
                "nb_fetched_task",
                "The number of fetched tasks, during this loop.",
            )?,
            nb_done_task: IntCounter::new(
                "nb_done_task",
                "The number of done tasks, during this loop.",
            )?,
            nb_bouche_job: IntCounter::new(
                "nb_bouche_job",
                "The number of tasks passed on to bouche for notification, during this loop.",
            )?,
            distributed_nb: IntCounter::new("distributed_nb", "The number of tasks distributed.")?,
            nb_tasks_checked: IntGauge::new("nb_tasks_checked", "The number of tasks checked.")?,
        })
    }

    /// Register metrics
    pub fn register(&self, registry: &Registry) -> Result<(), InternalError> {
        registry.register(Box::new(self.run_tasks_hist.clone()))?;
        registry.register(Box::new(self.fetch_hist.clone()))?;
        registry.register(Box::new(self.done_hist.clone()))?;
        registry.register(Box::new(self.orphaned_hist.clone()))?;
        registry.register(Box::new(self.nb_tasks.clone()))?;
        registry.register(Box::new(self.nb_tasks_success.clone()))?;
        registry.register(Box::new(self.nb_tasks_failure.clone()))?;
        registry.register(Box::new(self.nb_tasks_skipped.clone()))?;
        registry.register(Box::new(self.nb_fetched_task.clone()))?;
        registry.register(Box::new(self.nb_done_task.clone()))?;
        registry.register(Box::new(self.nb_bouche_job.clone()))?;
        registry.register(Box::new(self.distributed_nb.clone()))?;
        registry.register(Box::new(self.nb_tasks_checked.clone()))?;
        registry.register(Box::new(self.saturation.clone()))?;
        registry.register(Box::new(self.db_check_active.clone()))?;
        registry.register(Box::new(self.db_check_updated.clone()))?;
        registry.register(Box::new(self.distributed_hist.clone()))?;
        registry.register(Box::new(self.db_check_done.clone()))?;
        registry.register(Box::new(IntCounter::new("up", "Up status about an oeil")?))?;
        Ok(())
    }
}

impl OeilTaskResult {
    pub fn new() -> Self {
        OeilTaskResult {
            success: 0,
            failed: 0,
            skipped: 0,
            notified: 0,
            badly_formatted: 0,
        }
    }

    pub fn add_success(&mut self) {
        self.success = self.success + 1;
    }

    pub fn add_failed(&mut self) {
        self.failed = self.failed + 1;
    }

    pub fn add_skipped(&mut self) {
        self.skipped = self.skipped + 1;
    }

    pub fn add_notified(&mut self) {
        self.notified = self.notified + 1;
    }

    pub fn add_badly_formatted(&mut self) {
        self.badly_formatted = self.badly_formatted + 1;
    }
}

impl OeilTDResult {
    pub fn new() -> Self {
        OeilTDResult {
            added: 0,
            updated: 0,
            removed: 0,
        }
    }

    pub fn add_added(&mut self) {
        self.added = self.added + 1;
    }

    pub fn add_updated(&mut self) {
        self.updated = self.updated + 1;
    }

    pub fn add_removed(&mut self) {
        self.removed = self.removed + 1;
    }

    pub fn add_added_by(&mut self, i: usize) {
        self.added = self.added + i;
    }

    pub fn add_updated_by(&mut self, i: usize) {
        self.updated = self.updated + i;
    }

    pub fn add_removed_by(&mut self, i: usize) {
        self.removed = self.removed + i;
    }
}

impl std::ops::Add<OeilTDResult> for OeilTDResult {
    type Output = OeilTDResult;

    fn add(self, rhs: OeilTDResult) -> OeilTDResult {
        let mut res = OeilTDResult::new();
        res.added = self.added + rhs.added();
        res.removed = self.removed + rhs.removed();
        res.updated = self.updated + rhs.updated();
        res
    }
}

impl std::ops::Add<OeilTaskResult> for OeilTaskResult {
    type Output = OeilTaskResult;

    fn add(self, rhs: OeilTaskResult) -> OeilTaskResult {
        let mut res = OeilTaskResult::new();
        res.failed = self.failed + rhs.failed();
        res.success = self.success + rhs.success();
        res.badly_formatted = self.badly_formatted + rhs.badly_formatted();
        res.skipped = self.skipped + rhs.skipped();
        res.notified = self.notified + rhs.notified();
        res
    }
}

impl OeilTask {
    pub fn new(status: OeilTaskStatus) -> Self {
        OeilTask { status }
    }
}

impl Oeil {
    pub async fn new(config: Configuration, metrics: OeilMetrics) -> Result<Self, InternalError> {
        Ok(Oeil {
            visage: Visage::new(config.visage, YEUX).await?,
            config: config.oeil,
            metrics,
        })
    }

    pub(super) fn metrics_process_result_task_run(&self, res: &OeilTaskResult) {
        self.metrics().nb_tasks_success().set(*res.success() as i64);
        self.metrics()
            .nb_tasks_failure()
            .inc_by(*res.failed() as i64);
        self.metrics()
            .nb_tasks_skipped()
            .inc_by(*res.skipped() as i64);
        self.metrics()
            .nb_bouche_job()
            .inc_by(*res.notified() as i64);
        self.metrics()
            .nb_tasks()
            .set(*res.success() as i64 + *res.skipped() as i64);
    }

    pub(super) fn metrics_process_result_task_management(&self, res: &OeilTDResult) {
        self.metrics()
            .nb_fetched_task()
            .inc_by(*res.added() as i64 + *res.updated() as i64);
        self.metrics().nb_done_task().inc_by(*res.removed() as i64);
    }
}
