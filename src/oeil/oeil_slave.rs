use super::*;
use db::activity_constraint;
use log::{info, warn};
use std::collections::HashSet;
use std::iter::FromIterator;

impl Oeil {
    /// Check if the task fetched from redis is valid
    ///
    /// # Arguments
    /// * `res` - The result to fill
    /// * `redis_token` - The `todo` pipe to use to set some query later
    /// * `list` - The list from which to fetch the tasks
    /// * `key` - The key to fetch from that list
    /// * `task` - The raw task that was fetch using the `key`
    ///
    /// # Return Value
    /// The parsed tasked or None
    async fn slave_parse_task(
        &self,
        res: &mut OeilTaskResult,
        redis_todo: &mut redis::Pipeline,
        list: &String,
        key: &String,
        task: &String,
    ) -> Result<Option<OeilConstraint>, InternalError> {
        let key = match Uuid::from_str(key.as_str()) {
            // Extract the key
            Ok(x) => x,
            Err(_e) => {
                res.add_badly_formatted();
                debug!(
                    "Badly formatted key ('{}') in oeil {} backlog",
                    key,
                    self.visage().id()
                );
                redis_todo.hdel(list.as_str(), key);
                return Ok(None);
            }
        };
        let task: OeilConstraint = match serde_json::from_str(task.as_str()) {
            // Extract the task
            Ok(x) => x,
            Err(_e) => {
                res.add_badly_formatted();
                debug!(
                    "Badly formatted object ('{}') in oeil {} backlog",
                    key,
                    self.visage().id()
                );
                self.add_to_slave_update_bl(self.visage().id(), &key)
                    .await?;
                return Ok(None);
            }
        };
        if key != task.constraint().id {
            res.add_badly_formatted();
            debug!(
                "Badly formatted object ('{}') in oeil {} backlog",
                key,
                self.visage().id()
            );
            self.add_to_slave_update_bl(self.visage().id(), &key)
                .await?;
            return Ok(None);
        }
        Ok(Some(task))
    }

    /// Fetch every task incrementally from redis and check them
    ///
    /// # Return Value
    /// Statistics about the run
    pub async fn slave_run_tasks<'a>(&'a self) -> Result<OeilTaskResult, InternalError> {
        self.slave_run_tasks_internal(&*self.visage().db_conn().get()?)
            .await
    }

    /// Fetch every task incrementally from redis and check them (internal)
    ///
    /// # Return Value
    /// Statistics about the run
    pub async fn slave_run_tasks_internal<'a>(
        &'a self,
        db_conn: &'a diesel::PgConnection,
    ) -> Result<OeilTaskResult, InternalError> {
        let _timer = self.metrics().run_tasks_hist().start_timer();
        let mut res = OeilTaskResult::new();
        let mut index = 0;
        let list = YEUX_OEIL_BACKLOG_FORMAT!(self.visage().id());

        while {
            let mut todo = redis::pipe();
            // Scan for the 10 next element of the hashset
            let (new_index, tasks): (u64, HashMap<String, String>) = self
                .visage()
                .apply_cmd_in_redis(
                    redis::cmd("HSCAN")
                        .arg(list.as_str())
                        .arg(index)
                        .arg("COUNT")
                        .arg(10 as i64),
                )
                .await?;
            index = new_index;
            // Run the routine
            for (key, task) in tasks {
                let task: OeilConstraint = match self
                    .slave_parse_task(&mut res, &mut todo, &list, &key, &task)
                    .await?
                {
                    Some(x) => x,
                    None => continue,
                };
                res = res
                    + match task.constraint().type_ {
                        db::enums::ActivityConstraintType::Timer => {
                            self.check_constraint_timer(&db_conn, &mut todo, &list, task)
                                .await?
                        }
                    };
            }
            // Apply all the processed tasks
            self.visage().apply_pipe_in_redis(&mut todo).await?;
            // Quit the loop when the cursor is 0 (end)
            index != 0
        } {}
        self.metrics_process_result_task_run(&res);
        Ok(res)
    }

    /// Check if new/to-update tasks are available or if some tasks were marked as done
    ///
    /// # Arguments
    /// * `db_conn` - The connection to the database
    /// * `check_type` - The type of check the slave should operate
    ///
    /// # Return Value
    /// Statistics about the run
    pub(crate) async fn slave_check_loop_internal<'a>(
        &'a self,
        db_conn: &'a diesel::PgConnection,
        check_type: OeilSlaveCheckType,
    ) -> Result<OeilTDResult, InternalError> {
        let mut res = OeilTDResult::new();
        let mut index = 0;
        let _timer;
        let bl: String = YEUX_OEIL_BACKLOG_FORMAT!(self.visage().id());
        let old_list = match check_type {
            OeilSlaveCheckType::New => {
                _timer = self.metrics().fetch_hist().start_timer();
                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(self.visage().id())
            }
            OeilSlaveCheckType::Done => {
                _timer = self.metrics().done_hist().start_timer();
                YEUX_OEIL_BACKLOG_DONE_FORMAT!(self.visage().id())
            }
        };
        let list = self.visage().rename_to_random(old_list).await?;
        match &list {
            Some(list) => {
                while {
                    let mut todo = redis::pipe();
                    // Scan for the 10 next element of the hashset
                    let (new_index, tasks): (u64, Vec<String>) = self
                        .visage()
                        .apply_cmd_in_redis(
                            redis::cmd("SSCAN")
                                .arg(list.as_str())
                                .arg(index)
                                .arg("COUNT")
                                .arg(10 as i64),
                        )
                        .await?;
                    index = new_index;
                    // Run the routine
                    let run_res = match check_type {
                        OeilSlaveCheckType::New => {
                            self.slave_check_new_routine_internal(
                                db_conn,
                                &(tasks as Vec<String>),
                                &mut todo,
                                &list,
                            )
                            .await?
                        }
                        OeilSlaveCheckType::Done => {
                            self.slave_check_done_routine_internal(
                                db_conn,
                                &(tasks as Vec<String>),
                                &mut todo,
                                &list,
                            )
                            .await?
                        }
                    };
                    let sb = self
                        .visage()
                        .apply_cmd_in_redis::<u64>(&mut redis::Cmd::hlen(bl.as_str()))
                        .await?;
                    // Apply all the processed tasks
                    self.visage().apply_pipe_in_redis(&mut todo).await?;
                    let sa = self
                        .visage()
                        .apply_cmd_in_redis::<u64>(&mut redis::Cmd::hlen(bl.as_str()))
                        .await?;
                    let delta: i128 = sa as i128 - sb as i128;
                    match delta {
                        std::i128::MIN..=0 => res.add_removed_by((delta * -1) as usize), // Removed
                        1..=std::i128::MAX => {
                            res.add_added_by(*run_res.added()); // Added
                            res.add_updated_by(*run_res.added() - delta as usize);
                            // Updated
                        }
                    };
                    // Quit the loop when the cursor is 0 (end)
                    index != 0
                } {}
                self.visage()
                    .apply_cmd_in_redis(&mut redis::Cmd::del(list))
                    .await?;
            }
            None => (),
        };
        self.metrics_process_result_task_management(&res);
        Ok(res)
    }

    /// Check if new/to-update tasks are available
    ///
    /// # Arguments
    /// * `db_conn` - The connection to the database
    /// * `tasks` - A vector of raw tasks to parse
    /// * `todo` - The redis pipeline to change the state of the cache
    /// * `list` - The list from which the task were picked
    ///
    /// # Return Value
    /// Statistics about the run
    async fn slave_check_new_routine_internal(
        &self,
        db_conn: &diesel::PgConnection,
        tasks: &Vec<String>,
        todo: &mut redis::Pipeline,
        list: &String,
    ) -> Result<OeilTDResult, InternalError> {
        let mut res = OeilTDResult::new();
        let mut ids: Vec<Uuid> = Vec::new();
        let bl: String = YEUX_OEIL_BACKLOG_FORMAT!(self.visage().id());
        let mut task_map: HashSet<String> = HashSet::from_iter(tasks.iter().cloned());
        for task in tasks {
            match Uuid::from_str(task.as_str()) {
                Ok(id) => {
                    info!("Adding task {}", id);
                    ids.push(id);
                }
                Err(_e) => {
                    // Bad uuid
                    warn!("{}: Bad uuid ({}), removing", self.visage().id(), task);
                    todo.srem(list, task);
                }
            }
        }
        let data: Vec<activity_constraint::SelectActivityConstraint> =
            activity_constraint::select_bulk_by_id_unlogged(db_conn, ids)?;
        for r in data.into_iter() {
            let id = r.id.to_string();
            todo.hset(
                bl.as_str(),
                id.as_str(),
                serde_json::to_string(&OeilConstraint::new(r))?,
            );
            todo.srem(list, id.as_str());
            task_map.remove(&id);
            res.add_added();
        }
        for v in task_map.iter() {
            let id = Uuid::from_str(v.as_str());
            match id {
                Ok(x) => {
                    self.set_assigned_oeil(&x, OeilTaskStatus::Done(*self.visage().id()))
                        .await?;
                }
                Err(_e) => (),
            };
            res.add_removed();
        }
        Ok(res)
    }

    /// Check if some tasks are marked as done
    ///
    /// # Arguments
    /// * `db_conn` - The connection to the database
    /// * `tasks` - A vector of raw tasks to parse
    /// * `todo` - The redis pipeline to change the state of the cache
    /// * `list` - The list from which the task were picked
    ///
    /// # Return Value
    /// Statistics about the run
    async fn slave_check_done_routine_internal(
        &self,
        _db_conn: &diesel::PgConnection,
        tasks: &Vec<String>,
        todo: &mut redis::Pipeline,
        list: &String,
    ) -> Result<OeilTDResult, InternalError> {
        let mut res = OeilTDResult::new();
        let bl: String = YEUX_OEIL_BACKLOG_FORMAT!(self.visage().id());
        let blu: String = YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(self.visage().id());
        for task in tasks {
            match Uuid::from_str(task.as_str()) {
                Ok(id) => {
                    info!("{}: Removing task {}", self.visage().id(), id);
                    todo.hdel(bl.as_str(), id.to_string());
                    todo.srem(list, id.to_string());
                    todo.srem(blu.as_str(), id.to_string());
                    res.add_removed();
                }
                Err(_e) => {
                    // Bad uuid
                    warn!("{}: Bad uuid ({}), removing", self.visage().id(), task);
                    todo.srem(list, task);
                }
            }
        }
        Ok(res)
    }

    /// Run the loop when the slave for new/to-update tasks or tasks marked as done
    ///
    /// # Arguments
    /// * `check_type` - The type of check to do
    ///
    /// # Return Value
    /// Statistics about the run
    pub async fn slave_check_loop<'a>(
        &'a self,
        check_type: OeilSlaveCheckType,
    ) -> Result<OeilTDResult, InternalError> {
        self.slave_check_loop_internal(&*self.visage().db_conn().get()?, check_type)
            .await
    }
}
