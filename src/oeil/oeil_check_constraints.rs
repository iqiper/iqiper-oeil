use super::*;
use log::{info, warn};

impl Oeil {
    /// Distribute the jobs that are not assigned yet.
    ///
    /// # Arguments
    /// * `db_conn` - The connection to the database
    /// * `redis_todo` - The redis pipeline to use to apply modification
    /// * `list` - The list from which the task was taken
    /// * `task` - The task to check
    ///
    ///
    /// # Return Value
    /// A result telling if the constraint has been a
    /// success or if a notification / an alert should be sent
    pub(super) async fn check_constraint_timer(
        &self,
        db_conn: &diesel::PgConnection,
        redis_todo: &mut redis::Pipeline,
        list: &String,
        task: OeilConstraint,
    ) -> Result<OeilTaskResult, InternalError> {
        let now = Utc::now();
        let mut res = OeilTaskResult::new();
        let mut task = task;

        if task.constraint().start_date > now {
            // Timer not started yet
            debug!(
                "Task {} is skipped ({} < {})",
                task.constraint().id,
                task.constraint().start_date,
                now
            );
            res.add_skipped();
            return Ok(res);
        } else
        // task.constraint().start_date <= now
        // Started
        {
            let notify_date = match task.constraint().notify_date {
                // Fails if the notify_date is None
                Some(x) => x,
                None => {
                    res.add_badly_formatted();
                    warn!(
                        "Badly formatted object ('{}') in oeil {} backlog",
                        task.constraint().id,
                        self.visage().id()
                    );
                    self.add_to_slave_update_bl(self.visage().id(), &task.constraint().id)
                        .await?;
                    return Ok(res);
                }
            };
            // If it's not failed, but later than the check date, and we've never sent a notification
            if !task.notified() && notify_date <= now && task.constraint().end_date > now {
                info!("Task {} has been notified", task.constraint().id,);
                redis_todo.add_command(self.add_to_bouche_bl(
                    &db_conn,
                    &task.constraint().id,
                    CommunicationReasonType::Check,
                )?);
                task.set_notified(true);
                redis_todo.hset(
                    list.as_str(),
                    task.constraint().id.to_string(),
                    serde_json::to_string(&task)?,
                );
                res.add_notified();
            }
            if task.constraint().end_date <= now {
                // The constraint is failed
                info!("Task {} is a failure", task.constraint().id,);
                redis_todo.add_command(self.add_to_bouche_bl(
                    &db_conn,
                    &task.constraint().id,
                    CommunicationReasonType::Alert,
                )?);
                self.set_assigned_oeil(
                    &task.constraint().id,
                    OeilTaskStatus::Done(*self.visage().id()),
                )
                .await?;
                res.add_failed();
            } else {
                // The constraint is fine
                debug!("Task {} is a success", task.constraint().id);
                res.add_success();
            }
            Ok(res)
        }
    }
}
