use super::*;

impl Oeil {
    /// Check if some `oeil` as gone missing. If so re-assign its tasks and remove any traces of it.
    ///
    /// # Return Value
    /// A vector of uuid of dead `oeil`
    pub async fn master_check_orphaned_oeil(&self) -> Result<Vec<Uuid>, InternalError> {
        let _timer = self.metrics().orphaned_hist().start_timer();
        debug!("Checking for orphaned oeil");
        let yeux: Vec<String> = self
            .visage()
            .apply_cmd_in_redis(
                redis::cmd("ZRANGE")
                    .arg(YEUX)
                    .arg(0 as usize)
                    .arg(-1 as i64),
            ) // TODO Use ZSCAN
            .await?;
        let mut check_command = redis::pipe();
        for oeil_id in yeux.iter() {
            check_command.exists(visage::VISAGE_ID_FORMAT!(oeil_id));
        }
        let existing_oeil_map: Vec<bool> = self
            .visage()
            .apply_pipe_in_redis(&mut check_command)
            .await?;
        let mut res: Vec<Uuid> = vec![];
        for (oeil_id_formatted, exists) in yeux.iter().zip(existing_oeil_map.iter()) {
            if !exists {
                match Uuid::from_str(oeil_id_formatted) {
                    Ok(id) => res.push(id),
                    Err(_e) => {
                        self.visage()
                            .apply_cmd_in_redis(&mut redis::Cmd::zrem(YEUX, oeil_id_formatted))
                            .await?
                    }
                };
            }
        }
        for oeil_id in res.iter() {
            self.visage().unregister_lease(Some(*oeil_id)).await?;
        }
        debug!(
            "Delete {} oeil(s){}{}",
            res.len(),
            match res.len() {
                0 => "",
                _ => " :\n\t- ",
            },
            res.iter()
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
                .join("\n\t- ")
        );
        Ok(res)
    }
}
