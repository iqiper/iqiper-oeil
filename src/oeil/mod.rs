#[macro_export]
macro_rules! YEUX_OEIL_BACKLOG_FORMAT {
    ($oeil_id:expr) => {
        format!("BL_{}", $oeil_id)
    };
}

#[macro_export]
macro_rules! YEUX_OEIL_BACKLOG_UPDATE_FORMAT {
    ($oeil_id:expr) => {
        format!("BLU_{}", $oeil_id)
    };
}

#[macro_export]
macro_rules! YEUX_OEIL_BACKLOG_DONE_FORMAT {
    ($oeil_id:expr) => {
        format!("BLD_{}", $oeil_id)
    };
}

mod oeil;
mod oeil_check_constraints;
mod oeil_master;
mod oeil_registration;
mod oeil_slave;
mod oeil_utils;
use crate::errors::InternalError;
use chrono::{DateTime, Utc};
use visage::Visage;

use crate::configuration::Configuration;
use db::enums::CommunicationReasonType;
use getset::{Getters, MutGetters, Setters};
use log::{debug, error};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::str::FromStr;
use std::string::ToString;
use strum_macros::Display;
use uuid::Uuid;
pub const YEUX: &str = "Y";
pub const YEUX_BACKLOG: &str = "YBL";
pub const YEUX_FULL_UPDATE_TS: &str = "yeux_full_upt_since";
pub const YEUX_INACTIVE_UPDATE_TS: &str = "yeux_inactive_upt_since";
pub const YEUX_UPDATED_UPDATE_TS: &str = "yeux_updated_upt_since";
pub const YEUX_ACTIVE_UPDATE_TS: &str = "yeux_active_upt_since";

pub use oeil::{
    Oeil, OeilConstraint, OeilDBCheckType, OeilMetrics, OeilSlaveCheckType, OeilTDResult, OeilTask,
    OeilTaskResult, OeilTaskStatus,
};
