use super::*;
use log::warn;

impl Oeil {
    /// Used to fetched new/to-update/to-delete jobs from the database
    ///
    /// # Arguments
    /// * `db_check_type` - The type of check to run
    /// * `db_conn` - The database connection
    /// * `since` - From what timestamp begin reading the database
    ///
    /// # Return Value
    /// The number of records affecting the state of `redis`
    async fn db_check_routine(
        &self,
        db_check_type: OeilDBCheckType,
        db_conn: &diesel::PgConnection,
        since: &DateTime<Utc>,
    ) -> Result<usize, InternalError> {
        let mut res: usize = 0;
        let mut last_constraint_details = None;
        let status_formatted = serde_json::to_string(&OeilTask::new(OeilTaskStatus::Inactive))?;
        let _timer = match db_check_type {
            OeilDBCheckType::Full => self.metrics().db_check_active().start_timer(),
            OeilDBCheckType::Active => self.metrics().db_check_active().start_timer(),
            OeilDBCheckType::Inactive => self.metrics().db_check_done().start_timer(),
            OeilDBCheckType::Updated => self.metrics().db_check_updated().start_timer(),
        };
        while {
            let constraints = match db_check_type {
                OeilDBCheckType::Full => {
                    db::activity_constraint::select_all_active_paginated_unlogged(
                        db_conn,
                        last_constraint_details,
                        self.visage().config().database.read_step as i64,
                    )?
                }
                OeilDBCheckType::Active => {
                    db::activity_constraint::select_all_active_since_paginated_unlogged(
                        db_conn,
                        *since,
                        last_constraint_details,
                        self.visage().config().database.read_step as i64,
                    )?
                }
                OeilDBCheckType::Inactive => {
                    db::activity_constraint::select_all_inactive_since_paginated_unlogged(
                        db_conn,
                        *since,
                        last_constraint_details,
                        self.visage().config().database.read_step as i64,
                    )?
                }
                OeilDBCheckType::Updated => {
                    db::activity_constraint::select_all_updated_since_paginated_unlogged(
                        db_conn,
                        *since,
                        last_constraint_details,
                        self.visage().config().database.read_step as i64,
                    )?
                }
            };
            let mut todo = redis::pipe();
            for constraint in constraints.iter() {
                match db_check_type {
                    OeilDBCheckType::Updated => {
                        // If assigned and updated, add to the `to update` list
                        match self.get_assigned_oeil(&constraint.id).await? {
                            Some(status) => {
                                match status {
                                    OeilTaskStatus::Active(oeil_id)
                                    | OeilTaskStatus::Done(oeil_id) => {
                                        self.visage()
                                            .apply_cmd_in_redis(&mut redis::Cmd::sadd(
                                                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil_id),
                                                constraint.id.to_string(),
                                            ))
                                            .await?;
                                    }
                                    OeilTaskStatus::Inactive => {
                                        // Do nothing, it's not yet active.
                                    }
                                }
                            }
                            None => {
                                // Shouldn't arrive here. Means we fetched the status of an unknown record
                                todo.hdel(YEUX_BACKLOG, constraint.id.to_string());
                            }
                        }
                    }
                    OeilDBCheckType::Inactive => {
                        let status: Option<OeilTaskStatus> =
                            self.get_assigned_oeil(&constraint.id).await?;
                        match status {
                            Some(status) => match status {
                                OeilTaskStatus::Active(oeil_id) | OeilTaskStatus::Done(oeil_id) => {
                                    todo.add_command(self.set_assigned_oeil_internal(
                                        &constraint.id,
                                        OeilTaskStatus::Done(oeil_id),
                                    )?);
                                }
                                OeilTaskStatus::Inactive => {
                                    todo.hdel(YEUX_BACKLOG, constraint.id.to_string());
                                }
                            },
                            None => {
                                // Shouldn't arrive here. Means we fetched the status of an unknown record
                                todo.hdel(YEUX_BACKLOG, constraint.id.to_string());
                            }
                        };
                    }
                    _ => {
                        todo.hset(
                            YEUX_BACKLOG,
                            constraint.id.to_string(),
                            status_formatted.as_str(),
                        );
                    }
                }
            }
            let constraint_len: i128 = (constraints.len() as i128) - 1;
            if constraint_len >= 0 {
                let last_constraint = &constraints[constraint_len as usize];
                last_constraint_details = Some((last_constraint.id, last_constraint.created_at));
            }
            if constraint_len >= 0 {
                self.visage().apply_pipe_in_redis(&mut todo).await?;
                res = res + constraint_len as usize + 1;
            }
            constraints.len() != 0
        } {}
        Ok(res)
    }

    /// Used to fetched new/to-update/to-delete jobs from the database (internal)
    pub(crate) async fn master_db_check_internal(
        &self,
        check_type: OeilDBCheckType,
        db_conn: &diesel::PgConnection,
    ) -> Result<usize, InternalError> {
        let date_begin = Utc::now();
        let mut options: (&str, Vec<&str>) = match check_type {
            OeilDBCheckType::Full => (YEUX_FULL_UPDATE_TS, vec![]),
            OeilDBCheckType::Active => (
                YEUX_ACTIVE_UPDATE_TS,
                vec![YEUX_ACTIVE_UPDATE_TS, YEUX_FULL_UPDATE_TS],
            ),
            OeilDBCheckType::Inactive => (
                YEUX_INACTIVE_UPDATE_TS,
                vec![YEUX_INACTIVE_UPDATE_TS, YEUX_FULL_UPDATE_TS],
            ),
            OeilDBCheckType::Updated => (
                YEUX_UPDATED_UPDATE_TS,
                vec![YEUX_UPDATED_UPDATE_TS, YEUX_FULL_UPDATE_TS],
            ),
        };
        let since = self
            .master_get_time_check(&date_begin, &mut options.1)
            .await?;
        let res: usize = self.db_check_routine(check_type, db_conn, &since).await?;
        self.master_set_time_check(options.0, date_begin).await?;
        Ok(res)
    }

    /// Used to fetched new/to-update/to-delete jobs from the database
    ///
    /// # Arguments
    /// * `db_check_type` - The type of check to run
    ///
    /// # Return Value
    /// The number of records affecting the state of `redis
    pub(crate) async fn master_db_check(
        &self,
        check_type: OeilDBCheckType,
    ) -> Result<usize, InternalError> {
        self.master_db_check_internal(check_type, &*self.visage().db_conn().get()?)
            .await
    }

    /// Distribute the jobs that are not assigned yet.
    ///
    /// # Return Value
    /// Statistics about the run
    pub(crate) async fn master_distribute_tasks(&self) -> Result<OeilTDResult, InternalError> {
        let mut res = OeilTDResult::new();
        let mut index = 0;
        let mut total_nb_tasks = 0;
        let _time = self.metrics().distributed_hist().start_timer();
        while {
            let mut todo = redis::pipe();
            // Fetch a sub list from redis
            let lightest_oeil: Uuid = match self.visage().get_lightest_visage().await? {
                Some(x) => x,
                None => {
                    error!("No valid lightest_oeil found, quitting task distribution");
                    return Ok(OeilTDResult::new());
                }
            };
            if let Some(visage::VisageRole::Master) =
                self.visage().get_visage_role(&lightest_oeil).await?
            {
                warn!("Only master oeil is available, this stops the task distribution process");
            }
            // Scan for the 10 next element of the hashset
            let (new_index, tasks): (u64, HashMap<String, String>) = self
                .visage()
                .apply_cmd_in_redis(
                    redis::cmd("HSCAN")
                        .arg(YEUX_BACKLOG)
                        .arg(index)
                        .arg("COUNT")
                        .arg(10 as usize),
                )
                .await?;
            index = new_index;
            for (task_id_unparsed, task_obj) in tasks.iter() {
                total_nb_tasks = total_nb_tasks + 1;
                let task_id: Option<Uuid> = match Uuid::from_str(task_id_unparsed.as_str()) {
                    Ok(id) => Some(id),
                    Err(_e) => None,
                };
                match task_id {
                    Some(task_id) => {
                        match serde_json::from_str::<OeilTask>(task_obj.as_str()) {
                            Ok(task) => match task.status() {
                                OeilTaskStatus::Active(oeil_id) => {
                                    // If the task is already assigned to a dead oeil, check that it's now unassigned
                                    let exists: Option<usize> =
                                        self.visage().get_visage_score(&oeil_id).await?;
                                    let role: Option<visage::VisageRole> =
                                        self.visage().get_visage_role(&oeil_id).await?;
                                    match role {
                                        Some(visage::VisageRole::Both)
                                        | Some(visage::VisageRole::Slave) => (),
                                        Some(visage::VisageRole::Master) => {
                                            warn!("Task {} should'nt be assigned to a master oeil. It'll be requeued", task_id);
                                            todo.add_command(self.set_assigned_oeil_internal(
                                                &task_id,
                                                OeilTaskStatus::Inactive,
                                            )?);
                                            todo.hdel(
                                                YEUX_OEIL_BACKLOG_FORMAT!(oeil_id),
                                                task_id.to_string(),
                                            );
                                            todo.srem(
                                                YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil_id),
                                                task_id.to_string(),
                                            );
                                            todo.srem(
                                                YEUX_OEIL_BACKLOG_DONE_FORMAT!(oeil_id),
                                                task_id.to_string(),
                                            );
                                        }
                                        None => {
                                            warn!("Task {} should'nt be assigned to an oeil with no role. It'll be requeued", task_id);
                                            todo.add_command(self.set_assigned_oeil_internal(
                                                &task_id,
                                                OeilTaskStatus::Inactive,
                                            )?);
                                        }
                                    }
                                    match exists {
                                        Some(_x) => {
                                            // Task exists in oeil
                                            let exists: bool = self
                                                .visage()
                                                .apply_cmd_in_redis(&mut redis::Cmd::hexists(
                                                    YEUX_OEIL_BACKLOG_FORMAT!(oeil_id),
                                                    task_id.to_string(),
                                                ))
                                                .await?;
                                            // If it doesn't add to the to add/update list
                                            if !exists {
                                                debug!("{} not in oeil {}", task_id, oeil_id);
                                                todo.sadd(
                                                    YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(oeil_id),
                                                    task_id.to_string(),
                                                );
                                                res.add_updated();
                                            }
                                        }
                                        None => {
                                            // If the oeil doesn't exists, set the task as unassigned
                                            debug!(
                                                "{} oeil is dead, putting {} back in the BL",
                                                oeil_id, task_id
                                            );
                                            todo.add_command(self.set_assigned_oeil_internal(
                                                &task_id,
                                                OeilTaskStatus::Inactive,
                                            )?);
                                            res.add_updated();
                                        }
                                    }
                                }
                                OeilTaskStatus::Done(oeil_id) => {
                                    let exists: Option<usize> =
                                        self.visage().get_visage_score(&oeil_id).await?;
                                    // Delete it from the backlog
                                    todo.hdel(YEUX_BACKLOG, task_id.to_string());
                                    // Decrease its score
                                    if exists.is_some() {
                                        // If oeil exists, add the task to its done line
                                        todo.sadd(
                                            YEUX_OEIL_BACKLOG_DONE_FORMAT!(oeil_id),
                                            task_id.to_string(),
                                        );
                                        if self.check_if_assigned(oeil_id, &task_id).await? {
                                            todo.zincr(YEUX, oeil_id.to_string(), -1);
                                        }
                                    }
                                    res.add_removed();
                                }
                                OeilTaskStatus::Inactive => {
                                    // If the task is not assigned, give it to lightest oeil
                                    debug!("Assigning {} to {}", task_id, lightest_oeil);
                                    todo.add_command(self.set_assigned_oeil_internal(
                                        &task_id,
                                        OeilTaskStatus::Active(lightest_oeil),
                                    )?);
                                    todo.sadd(
                                        YEUX_OEIL_BACKLOG_UPDATE_FORMAT!(lightest_oeil),
                                        task_id.to_string(),
                                    );
                                    // Increase its score
                                    todo.zincr(YEUX, lightest_oeil.to_string(), 1);
                                    res.add_added();
                                }
                            },
                            Err(_e) => {
                                res.add_updated();
                                // If the struct is badly formatted, default to inactive
                                todo.add_command(self.set_assigned_oeil_internal(
                                    &task_id,
                                    OeilTaskStatus::Inactive,
                                )?);
                            }
                        };
                    }
                    None => {
                        // If the key is not a valid Uuid, delete it
                        todo.hdel(YEUX_BACKLOG, task_id_unparsed);
                        res.add_removed();
                    }
                };
            }
            self.metrics()
                .distributed_nb()
                .inc_by(*res.added() as i64 + *res.removed() as i64 + *res.updated() as i64);
            self.metrics().nb_tasks_checked().set(total_nb_tasks);
            // Apply all the processed tasks
            self.visage().apply_pipe_in_redis(&mut todo).await?;
            // Quit the loop when the cursor is 0 (end)
            index != 0
        } {}
        Ok(res)
    }
}
