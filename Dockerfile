FROM gcr.io/distroless/static:nonroot

COPY --chown=nonroot:nonroot ./target/x86_64-unknown-linux-musl/release/iqiper-oeil /app/iqiper-oeil

EXPOSE 8000 9000

CMD ["/app/iqiper-oeil"]
