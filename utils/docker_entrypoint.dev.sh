#!/bin/bash -x

/usr/src/oeil/target/x86_64-unknown-linux-musl/debug/iqiper-oeil &
while true;
do
	inotifywait /.restart
	kill %%
	wait %%
	/usr/src/oeil/target/x86_64-unknown-linux-musl/debug/iqiper-oeil &
done

